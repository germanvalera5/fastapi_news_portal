from pydantic import BaseSettings, EmailStr


class Settings(BaseSettings):
    API_V1_STR: str = "/api/v1"
    SECRET_KEY: str = '230786f5b266f3d5f56855a6bd97c9c2ee1f3d2ebd0263c27d71923f44d078f4'
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 30
    PROJECT_NAME: str = 'Social network'
    SQLALCHEMY_DATABASE_URL: str = 'postgresql://admin:123456@localhost/social_network'
    FIRST_SUPERUSER: EmailStr = 'superuser@example.com'
    FIRST_SUPERUSER_PASSWORD: str = 'somelongpass'
    USERNAME_TEST_USER: str = 'testuser'
    ALGORITHM: str = "HS256"


settings = Settings()
