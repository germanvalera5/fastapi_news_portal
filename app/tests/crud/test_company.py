from sqlalchemy.orm import Session
from app import crud
from app.schemas.company import CompanyCreate, CompanyUpdate
from app.tests.utils.utils import random_lower_string


def test_create_company(db: Session) -> None:
    name = random_lower_string()
    address = random_lower_string()
    company_in = CompanyCreate(name=name, address=address)
    company = crud.create_company(db=db, company=company_in)
    assert company.name == name
    assert company.address == address


def test_get_company(db: Session) -> None:
    name = random_lower_string()
    address = random_lower_string()
    company_in = CompanyCreate(name=name, address=address)
    company = crud.create_company(db=db, company=company_in)
    company1 = crud.get_company(db=db, name=name)
    assert company1
    assert company1.name == company.name
    assert company1.address == company.address


def test_update_company(db: Session) -> None:
    name = random_lower_string()
    address = random_lower_string()
    company_in = CompanyCreate(name=name, address=address)
    company = crud.create_company(db=db, company=company_in)
    name2 = random_lower_string()
    company_update = CompanyUpdate(name=name2)
    company_data = company_update.dict(exclude_unset=True)
    for key, value in company_data.items():
        setattr(company, key, value)
    db.add(company)
    db.commit()
    db.refresh(company)
    assert company.name == name2


def test_delete_company(db: Session) -> None:
    name = random_lower_string()
    address = random_lower_string()
    company_in = CompanyCreate(name=name, address=address)
    company = crud.create_company(db=db, company=company_in)
    company2 = crud.delete_company(db=db, name=name)
    company3 = crud.get_company(db=db, name=name)
    assert company3 is None
    assert company2.id == company.id
    assert company2.name == name
    assert company2.address == address

