from sqlalchemy.orm import Session

from app import crud
from app.schemas.post import PostCreate, PostUpdate
from app.tests.utils.user import create_random_user
from app.tests.utils.company import create_random_company
from app.tests.utils.utils import random_lower_string


def test_create_post(db: Session) -> None:
    title = random_lower_string()
    text = random_lower_string()
    topic = random_lower_string()
    post_in = PostCreate(title=title, text=text, topic=topic)
    company = create_random_company(db=db)
    company_id = company.id
    user = create_random_user(db=db, company_id=company_id)
    post = crud.create_user_post(db=db, user_id=user.id, post=post_in)
    assert post.title == title
    assert post.text == text
    assert post.author_id == user.id


def test_get_post(db: Session) -> None:
    title = random_lower_string()
    text = random_lower_string()
    topic = random_lower_string()
    post_in = PostCreate(title=title, text=text, topic=topic)
    company = create_random_company(db=db)
    company_id = company.id
    user = create_random_user(db=db, company_id=company_id)
    post = crud.create_user_post(db=db, user_id=user.id, post=post_in)
    stored_post = crud.get_post(db=db, post_id=post.id)
    assert stored_post
    assert post.id == stored_post.id
    assert post.title == stored_post.title
    assert post.text == stored_post.text
    assert post.author_id == stored_post.author_id


def test_update_post(db: Session) -> None:
    title = random_lower_string()
    text = random_lower_string()
    topic = random_lower_string()
    post_in = PostCreate(title=title, text=text, topic=topic)
    company = create_random_company(db=db)
    company_id = company.id
    user = create_random_user(db=db, company_id=company_id)
    post = crud.create_user_post(db=db, user_id=user.id, post=post_in)
    text2 = random_lower_string()
    post_update = PostUpdate(text=text2)
    post_data = post_update.dict(exclude_unset=True)
    for key, value in post_data.items():
        setattr(post, key, value)
    db.add(post)
    db.commit()
    db.refresh(post)
    assert post.text == text2


def test_delete_post(db: Session) -> None:
    title = random_lower_string()
    text = random_lower_string()
    topic = random_lower_string()
    post_in = PostCreate(title=title, text=text, topic=topic)
    company = create_random_company(db=db)
    company_id = company.id
    user = create_random_user(db=db, company_id=company_id)
    post = crud.create_user_post(db=db, user_id=user.id, post=post_in)
    post2 = crud.delete_post(db=db, post_id=post.id)
    post3 = crud.get_post(db=db, post_id=post.id)
    assert post3 is None
    assert post2.id == post.id
    assert post2.title == title
    assert post2.text == text
    assert post2.author_id == user.id
