from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app import crud
from app.dependencies import authenticate_user
from app.core.security import verify_password
from app.schemas.user import UserCreate, UserUpdate
from app.models.user import UserTypes
from app.tests.utils.utils import random_username, random_lower_string, random_email
from app.tests.utils.company import create_random_company


def test_create_user(db: Session) -> None:
    email = random_email()
    username = random_username()
    password = random_lower_string()
    company = create_random_company(db)
    company_id = company.id
    user_in = UserCreate(email=email, username=username,
                         password=password, company_id=company_id)
    user = crud.create_user(db=db, user=user_in)
    assert user.email == email
    assert hasattr(user, 'password')


def test_authenticate_user(db: Session) -> None:
    email = random_email()
    username = random_username()
    password = random_lower_string()
    company = create_random_company(db)
    company_id = company.id
    user_in = UserCreate(email=email, username=username,
                         password=password, company_id=company_id)
    user = crud.create_user(db=db, user=user_in)
    authenticated_user = authenticate_user(db, username, password)
    assert authenticated_user
    assert user.email == authenticated_user.email


def test_not_authenticate_user(db: Session) -> None:
    username = random_username()
    password = random_lower_string()
    user = authenticate_user(db, username=username, password=password)
    assert user is None


def test_check_if_user_is_superuser(db: Session) -> None:
    email=random_email()
    username = random_username()
    password = random_lower_string()
    company = create_random_company(db)
    company_id = company.id
    user_in = UserCreate(email=email, password=password,
                         username=username, company_id=company_id,
                         user_type=UserTypes.superuser)
    user = crud.create_user(db, user=user_in)
    assert user.user_type is UserTypes.superuser


def test_get_user(db: Session) -> None:
    password = random_lower_string()
    email = random_email()
    username = random_username()
    company = create_random_company(db)
    company_id = company.id
    user_in = UserCreate(username=username, email=email,
                         password=password, company_id=company_id)
    user = crud.create_user(db, user=user_in)
    user_2 = crud.get_user_by_id(db, user_id=user.id)
    assert user_2
    assert user.email == user_2.email
    assert jsonable_encoder(user) == jsonable_encoder(user_2)


def test_update_user(db: Session) -> None:
    password = random_lower_string()
    email = random_email()
    username = random_username()
    company = create_random_company(db)
    company_id = company.id
    user_in = UserCreate(username=username, email=email,
                         password=password, company_id=company_id)
    user = crud.create_user(db, user=user_in)
    user_in_update = UserUpdate(user_type=UserTypes.superuser)
    user_data = user_in_update.dict(exclude_unset=True)
    for key, value in user_data.items():
        setattr(user, key, value)
    db.add(user)
    db.commit()
    db.refresh(user)
    user_2 = crud.get_user_by_id(db, user_id=user.id)
    assert user_2
    assert user.email == user_2.email
    assert user.user_type == UserTypes.superuser
