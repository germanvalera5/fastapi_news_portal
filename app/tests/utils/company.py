from sqlalchemy.orm import Session

from app import crud
from app.models.company import Company
from app.schemas.company import CompanyCreate
from app.tests.utils.utils import random_lower_string


def create_random_company(db: Session) -> Company:
    name = random_lower_string()
    address = random_lower_string()
    company_in = CompanyCreate(name=name, address=address)
    company = crud.create_company(db, company_in)
    return company
