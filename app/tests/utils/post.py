from typing import Optional

from sqlalchemy.orm import Session

from app import crud, models
from app.schemas.post import PostCreate
from app.tests.utils.user import create_random_user
from app.tests.utils.utils import random_lower_string


def create_random_post(db: Session, *, author_id: Optional[int] = None) -> models.Post:
    if author_id is None:
        user = create_random_user(db)
        author_id = user.id
    title = random_lower_string()
    text = random_lower_string()
    topic = random_lower_string()
    post = PostCreate(title=title, text=text, topic=topic)
    return crud.create_user_post(db, post, author_id)