from typing import Dict, Optional

from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

from app import crud
from app.core.config import settings
from app.models.user import User
from app.schemas.user import UserCreate
from app.tests.utils.utils import random_lower_string, random_email, random_username
from app.tests.utils.company import create_random_company


def user_authentication_headers(
        *, client: TestClient, username: str, password: str
) -> Dict[str, str]:
    data = {'username': username, 'password': password}
    r = client.post(f'{settings.API_V1_STR}/users/token', data=data)
    response = r.json()
    auth_token = response['access_token']
    headers = {'Authorization': f'Bearer {auth_token}'}
    return headers


def create_random_user(db: Session, company_id: Optional[int] = None) -> User:
    email = random_email()
    username = random_username()
    password = random_lower_string()
    if company_id is None:
        company = create_random_company(db)
        company_id = company.id
    user_in = UserCreate(username=username, email=email,
                         password=password, company_id=company_id)
    user = crud.create_user(db=db, user=user_in)
    return user


def authentication_token_from_username(
    *, client: TestClient, username: str, db: Session
) -> Dict[str, str]:
    """
    Return a valid token for the user with given username.
    If the user doesn't exist it is created first.
    """
    password = random_lower_string()
    user = crud.get_user(db, username=username)
    if not user:
        user_in_create = UserCreate(username=username, email=username, password=password)
        user = crud.create_user(db, user=user_in_create)

    return user_authentication_headers(client=client, username=username, password=password)
