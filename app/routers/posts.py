from typing import List
from fastapi import APIRouter, HTTPException, Depends, status
from sqlalchemy.orm import Session

from ..schemas import Post, PostCreate, User, PostUpdate
from ..dependencies import get_current_user, get_db
from app.crud import crud_post

router = APIRouter(
    prefix='/posts',
    tags=['posts'],
    responses={404: {'description': 'Not found'}},
)


@router.get("/all", response_model=List[Post])
def read_all_posts(skip: int = 0, limit: int = 100,
                   db: Session = Depends(get_db)):
    items = crud_post.get_posts(db, skip=skip, limit=limit)
    return items


@router.get("/")
async def read_own_posts(db: Session = Depends(get_db),
                         current_user: User = Depends(get_current_user),
                         skip: int = 0, limit: int = 100):
    posts = crud_post.get_user_posts(db, current_user.id, skip=skip, limit=limit)
    if not posts:
        raise HTTPException(status_code=status.HTTP_204_NO_CONTENT,
                            detail='You have no posts.')
    return posts


@router.post("/", response_model=Post)
def create_post(post: PostCreate, db: Session = Depends(get_db),
                user: User = Depends(get_current_user)):
    return crud_post.create_user_post(db=db, post=post, user_id=user.id)


@router.get("/{post_id}", response_model=Post)
def read_post(post_id: int, db: Session = Depends(get_db)):
    post_db = crud_post.get_post(db, post_id)
    if post_db is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Post not found")
    return post_db


@router.put("/{post_id}", response_model=Post)
def update_post(post_id: int, post: PostUpdate,
                db: Session = Depends(get_db)):
    post = crud_post.update_post(db=db,
                                 post_id=post_id,
                                 post=post)
    return post


@router.delete("/{post_id}")
def delete_post(post_id: int, db: Session = Depends(get_db)):
    post = delete_post(post_id, db)
    return {'response': f'Post #{post.id} successfully deleted.'}
