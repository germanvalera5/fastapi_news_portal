from typing import List
from fastapi import APIRouter, Depends, HTTPException, status

from sqlalchemy.orm import Session
from ..schemas import CompanyCreate, Company, User, CompanyUpdate
from ..dependencies import get_current_user, get_db
from app.crud import crud_company

router = APIRouter(
    prefix='/companies',
    tags=['companies'],
    responses={404: {'description': 'Not found'}},
)


@router.get("/", response_model=List[Company])
def read_companies(skip: int = 0, limit: int = 100,
                   db: Session = Depends(get_db)):
    companies = crud_company.get_companies(db, skip=skip, limit=limit)
    return companies


@router.get("/myCompany", response_model=Company)
def read_my_company(db: Session = Depends(get_db),
                    current_user: User = Depends(get_current_user)):
    db_company = crud_company.get_user_company(db, company_id=current_user.company_id)
    if db_company is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="You are not in any of companies.")
    return db_company


@router.post("/", response_model=Company)
def company_create(company: CompanyCreate, db: Session = Depends(get_db)):
    db_company = crud_company.get_company(db, name=company.name)
    if db_company:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail="Company already registered")
    return crud_company.create_company(db=db, company=company)


@router.get("/{company_name}", response_model=Company)
def read_company(company_name: str, db: Session = Depends(get_db)):
    db_company = crud_company.get_company(db, name=company_name)
    if db_company is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Company not found")
    return db_company


@router.put('/{company_name}', response_model=Company)
def update_company(company_name: str,
                   company: CompanyUpdate,
                   db: Session = Depends(get_db)):
    company = crud_company.update_company(db=db,
                                          company_name=company_name,
                                          company=company)
    return company


@router.delete('/{company_name}')
def delete_company(company_name: str, db: Session = Depends(get_db)):
    company = delete_company(company_name, db)
    return {'response': f'Company {company.name} successfully deleted.'}
