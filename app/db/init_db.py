from sqlalchemy.orm import Session

from app import schemas
from app.core.config import settings
from app.crud.crud_user import get_user_by_email, create_user
from app.db.base_class import Base
from app.db.session import engine
from app.models import UserTypes


def init_db(db: Session):
    Base.metadata.create_all(bind=engine)

    user = get_user_by_email(db, email=settings.FIRST_SUPERUSER)
    if not user:
        user_in = schemas.UserCreate(
            email=settings.FIRST_SUPERUSER,
            password=settings.FIRST_SUPERUSER_PASSWORD,
            user_type=UserTypes.superuser,
        )
        user = create_user(db, user_in)
