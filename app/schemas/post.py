from typing import Optional
from pydantic import BaseModel


class PostBase(BaseModel):
    title: str
    text: str
    topic: str


class PostCreate(PostBase):
    pass


class Post(PostBase):
    id: int
    author_id: int

    class Config:
        orm_mode = True


class PostUpdate(BaseModel):
    title: Optional[str]
    text: Optional[str]
    topic: Optional[str]
