from datetime import datetime
from typing import Optional, List
from pydantic import BaseModel
from app.models import User
from .user import User


class CompanyCreate(BaseModel):
    name: str
    address: Optional[str]


class Company(BaseModel):
    id: int
    name: str
    address: Optional[str]
    date_created: datetime
    employees: List[User] = []

    class Config:
        orm_mode = True


class CompanyUpdate(BaseModel):
    name: Optional[str]
    address: Optional[str]
