from typing import Optional, List
from pydantic import BaseModel
from app.models import UserTypes, Post
from .post import Post


class UserCreate(BaseModel):
    email: str
    username: str
    password: str
    first_name: Optional[str] = 'John'
    last_name: Optional[str] = 'Doe'
    user_type: UserTypes = UserTypes.user
    company_id: int


class User(BaseModel):
    id: int
    email: str
    username: str
    first_name: str
    last_name: str
    user_type: UserTypes
    is_active: bool
    company_id: int
    posts: List[Post] = []

    class Config:
        orm_mode = True


class UserInDB(BaseModel):
    username: str
    email: Optional[str] = None
    password: str


class UserUpdate(BaseModel):
    email: Optional[str]
    username: Optional[str]
    first_name: Optional[str]
    last_name: Optional[str]
    user_type: Optional[UserTypes]
    company_id: Optional[int]
