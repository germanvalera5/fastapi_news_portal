from fastapi import FastAPI
from .routers import posts, users, companies
from app.core.config import settings


description = """
Social network API helps you do awesome stuff. 🚀

## Posts

You are able to :

* **Read all posts**.
* **Read your own posts**.
* **Read post**. 
* **Create post**.
* **Update post**.
* **Delete post**.

## Users

You are able to:

* **Create users**.
* **Read users**.
* **Read user by id**.
* **Update users**.
* **Delete users**.

## Companies

You are able to:

* **Create company**.
* **Read company**.
* **Read all companies**.
* **Update company**.
* **Delete company**.

"""

tags_metadata = [
    {
        "name": "users",
        "description": "Operations with users. The **login** logic is also here.",
    },
    {
        "name": "posts",
        "description": "Manage posts. So _fancy_ they have their own docs.",
    },
    {
        "name": "companies",
        "description": "Operations with companies.",
    },
]

app = FastAPI(
    title=settings.PROJECT_NAME,
    description=description,
    version="0.0.1",
    terms_of_service="http://example.com/terms/",
    contact={
        "name": "John Doe",
        "url": "http://x-force.example.com/contact/",
        "email": "dp@x-force.example.com",
    },
    license_info={
        "name": "Apache 2.0",
        "url": "https://www.apache.org/licenses/LICENSE-2.0.html",
    },
    openapi_tags=tags_metadata,
    openapi_url=f"{settings.API_V1_STR}/openapi.json",
)

app.include_router(posts.router, prefix=settings.API_V1_STR)
app.include_router(users.router, prefix=settings.API_V1_STR)
app.include_router(companies.router, prefix=settings.API_V1_STR)


@app.get("/")
async def home():
    return {"message": "Home"}
