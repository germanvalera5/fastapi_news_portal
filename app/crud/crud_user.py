from sqlalchemy.orm import Session

from fastapi import HTTPException, status

from app.models import User
from app.schemas import UserCreate, UserUpdate
from app.core.security import get_password_hash


def get_users(db: Session, skip: int = 0, limit: int = 100):
    return db.query(User).offset(skip).limit(limit).all()


def get_user(db: Session, username: str):
    return db.query(User).filter(User.username == username).first()


def get_user_by_id(db: Session, user_id: int):
    return db.query(User).filter(User.id == user_id).first()


def get_user_by_email(db: Session, email: str):
    return db.query(User).filter(User.email == email).first()


def create_user(db: Session, user: UserCreate):
    password = get_password_hash(user.password)
    db_user = User(email=user.email, password=password,
                   first_name=user.first_name, last_name=user.last_name,
                   company_id=user.company_id, user_type=user.user_type,
                   username=user.username)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def update_user(db: Session, user_id, user: UserUpdate):
    user_db = get_user_by_id(db=db, user_id=user_id)
    if user_db is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="User not found")
    user_data = user.dict(exclude_unset=True)
    for key, value in user_data.items():
        setattr(user_db, key, value)
    db.add(user_db)
    db.commit()
    db.refresh(user_db)
    return user_db


def delete_user(db: Session, user_id: int):
    user = get_user_by_id(db, user_id)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")
    db.delete(user)
    db.commit()
    return user
