from sqlalchemy.orm import Session

from fastapi import HTTPException, status

from app.models import Post
from app.schemas import PostCreate, PostUpdate


def get_posts(db: Session, skip: int = 0, limit: int = 100):
    return db.query(Post).offset(skip).limit(limit).all()


def get_post(db: Session, post_id: int):
    return db.query(Post).filter(Post.id == post_id).first()


def get_user_posts(db: Session, user_id: int,  skip: int = 0, limit: int = 100):
    return db.query(Post).filter(Post.author_id == user_id).offset(skip).limit(limit).all()


def create_user_post(db: Session, post: PostCreate, user_id: int):
    db_post = Post(**post.dict(), author_id=user_id)
    db.add(db_post)
    db.commit()
    db.refresh(db_post)
    return db_post


def update_post(db: Session, post_id: int, post: PostUpdate):
    post_db = get_post(db=db, post_id=post_id)
    if post_db is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Company not found")
    post_data = post.dict(exclude_unset=True)
    for key, value in post_data.items():
        setattr(post_db, key, value)
    db.add(post_db)
    db.commit()
    db.refresh(post_db)
    return post_db


def delete_post(db: Session, post_id: int):
    post = get_post(db, post_id)
    if not post:
        raise HTTPException(status_code=404, detail="Post not found")
    db.delete(post)
    db.commit()
    return post
