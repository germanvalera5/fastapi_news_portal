from sqlalchemy.orm import Session

from fastapi import HTTPException, status

from app.models import Company
from app.schemas import CompanyCreate, CompanyUpdate


def get_company(db: Session, name: str):
    return db.query(Company).filter(Company.name == name).first()


def get_user_company(db: Session, company_id: int):
    return db.query(Company).filter(Company.id == company_id).first()


def get_companies(db: Session, skip: int = 0, limit: int = 100):
    return db.query(Company).offset(skip).limit(limit).all()


def create_company(db: Session, company: CompanyCreate):
    db_company = Company(**company.dict())
    db.add(db_company)
    db.commit()
    db.refresh(db_company)
    return db_company


def update_company(db: Session, company_name: str, company: CompanyUpdate):
    company_db = get_company(db=db, name=company_name)
    if company_db is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Company not found")
    company_data = company.dict(exclude_unset=True)
    for key, value in company_data.items():
        setattr(company_db, key, value)
    db.add(company_db)
    db.commit()
    db.refresh(company_db)
    return company_db


def delete_company(db: Session, name: str):
    company = get_company(db, name)
    if not company:
        raise HTTPException(status_code=404, detail="Company not found")
    db.delete(company)
    db.commit()
    return company
