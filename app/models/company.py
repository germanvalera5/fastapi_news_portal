from typing import TYPE_CHECKING
from datetime import datetime

from sqlalchemy import Column, String, DateTime, Integer
from sqlalchemy.orm import relationship

from app.db.base_class import Base

if TYPE_CHECKING:
    from .user import User


class Company(Base):
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(length=100), nullable=False)
    address = Column(String(length=150))
    date_created = Column(DateTime, default=datetime.now())

    employees = relationship('User', back_populates='company')

    # url = models.URLField()
    # logo = models.ImageField(upload_to='logos/', null=True, blank=True)
