from typing import TYPE_CHECKING
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from app.db.base_class import Base

if TYPE_CHECKING:
    from .user import UserTypes


class Post(Base):
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String(length=100), nullable=False)
    text = Column(String, nullable=False)
    topic = Column(String, nullable=False)
    author_id = Column(Integer, ForeignKey('user.id',
                                           ondelete='CASCADE'))

    author = relationship('User', back_populates='posts')
