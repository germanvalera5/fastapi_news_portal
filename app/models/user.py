import enum
from typing import TYPE_CHECKING
from sqlalchemy import Integer, String, Column , Enum, Boolean, ForeignKey
from sqlalchemy.orm import relationship
from app.db.base_class import Base

if TYPE_CHECKING:
    from .company import Company
    from .post import Post


class UserTypes(enum.Enum):
    user = 'user'
    admin = 'admin'
    superuser = 'superuser'


class User(Base):
    id = Column(Integer, primary_key=True, index=True)
    email = Column(String, unique=True, index=True, nullable=False)
    username = Column(String, unique=True, index=True, nullable=True)
    password = Column(String)
    is_active = Column(Boolean, default=True)
    first_name = Column(String, nullable=True)
    last_name = Column(String, nullable=True)
    user_type = Column(Enum(UserTypes), nullable=False,
                       default=UserTypes.user)
    company_id = Column(Integer, ForeignKey('company.id',
                                            ondelete='SET NULL'))

    posts = relationship('Post', back_populates='author')
    company = relationship('Company', back_populates='employees')

    # avatar = models.ImageField(upload_to='avatars/', null=True, blank=True)
    # telephone_number = PhoneNumberField(blank=True)
